# Changelog
All notable changes to this project will be documented in this file!

## [0.0.2] - 2018-07-26
### Added
- CHANGELOG.md file

## [0.0.1] - 2018-07-26
### Added
- CHANGELOG.md file

### Commands
- `$ git-flow init -d`
- `$ git remote add origin git@bitbucket.org:dbi_dudas_nandor/test-git-flow.git`
- `$ git push -u origin master`
- `$ git push -u origin develop`
- `$ git checkout master`
- `$ echo '# Test git-flow' > README.md`
- `$ git add .`
- `$ git commit -m 'Add README file'`
- `$ git push -u origin master`
- `$ git-flow release start v0.0.1`
- `$ touch CHANGELOG.md`
- `$ git add .`
- `$ git commit -m 'Add CHANGELOG file'`
- `$ git-flow release finish v0.0.1 -m 'First release'`

[0.0.2]: https://bitbucket.org/dbi_dudas_nandor/test-git-flow/branches/compare/v0.0.2-alpha..v0.0.2
[0.0.1]: https://bitbucket.org/dbi_dudas_nandor/test-git-flow
