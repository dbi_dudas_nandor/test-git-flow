# Test git-flow

``` sh
$ git checkout master
$ git merge develop

$ git flow feature start this/is/another/very/deep/branch
$ git flow feature finish this/is/another/very/deep/branch --no-ff
```
